<?php

/**
 * This is the model class for table "usuarios".
 *
 * The followings are the available columns in table 'usuarios':
 * @property string $idUsuario
 * @property string $nomUsuario
 * @property string $apeUsuario
 * @property string $dirUsuario
 * @property string $userUsuario
 * @property string $passUsuario
 * @property string $emailUsuario
 */
class Usuarios extends CActiveRecord
{

public $verificarEmail;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Usuarios the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'usuarios';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nomUsuario, apeUsuario, dirUsuario, userUsuario, passUsuario, emailUsuario', 'required'),
			array('nomUsuario, apeUsuario, userUsuario, passUsuario', 'length', 'max'=>180),
			array('userUsuario,nomUsuario,emailUsuario','unique'),
			//array('emailUsuario','email')
			array('dirUsuario, emailUsuario', 'length', 'max'=>180),
			//array('verificarEmail','compare','compareAtribute'=>'emailUsuario'),
			// Please remove those attributes that should not be searched.
			array('idUsuario, nomUsuario, apeUsuario, dirUsuario, userUsuario, passUsuario, emailUsuario', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idUsuario' => 'Id Usuario',
			'nomUsuario' => 'Nombre Usuario',
			'apeUsuario' => 'Apellido Usuario',
			'dirUsuario' => 'Dirección Usuario',
			'userUsuario' => 'Usuario',
			'passUsuario' => 'Password',
			'emailUsuario' => 'Email Usuario',
			'verificarEmail'=>'Verificar Email',
		);
	}

	protected function beforeValidate(){
if ($this->isNewRecord){


	$t_hasher = new PasswordHash(8, true);
	$hash = $t_hasher->HashPassword($this->passUsuario);
	$this-> passUsuario=$hash;
	/*$this->userUsuario=0;
	$this->nomUsuario=0;
	$this->emailUsuario=0;
*/}
	return parent::beforeValidate();


	}




	public function validarPass($passwd){

		$t_hasher = new PasswordHash(8,TRUE);
		return $t_hasher->CheckPassword($passwd, $this->passUsuario);

	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idUsuario',$this->idUsuario,true);
		$criteria->compare('nomUsuario',$this->nomUsuario,true);
		$criteria->compare('apeUsuario',$this->apeUsuario,true);
		$criteria->compare('dirUsuario',$this->dirUsuario,true);
		$criteria->compare('userUsuario',$this->userUsuario,true);
		$criteria->compare('passUsuario',$this->passUsuario,true);
		$criteria->compare('emailUsuario',$this->emailUsuario,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
