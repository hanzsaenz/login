<?php
/* @var $this UsuariosController */
/* @var $model Usuarios */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'usuarios-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"> Los campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nomUsuario'); ?>
		<?php echo $form->textField($model,'nomUsuario',array('size'=>60,'maxlength'=>90)); ?>
		<?php echo $form->error($model,'nomUsuario'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'apeUsuario'); ?>
		<?php echo $form->textField($model,'apeUsuario',array('size'=>60,'maxlength'=>90)); ?>
		<?php echo $form->error($model,'apeUsuario'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dirUsuario'); ?>
		<?php echo $form->textField($model,'dirUsuario',array('size'=>60,'maxlength'=>180)); ?>
		<?php echo $form->error($model,'dirUsuario'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'userUsuario'); ?>
		<?php echo $form->textField($model,'userUsuario',array('size'=>60,'maxlength'=>90)); ?>
		<?php echo $form->error($model,'userUsuario'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'passUsuario'); ?>
		<?php echo $form->passwordField($model,'passUsuario',array('size'=>60,'maxlength'=>180)); ?>
		<?php echo $form->error($model,'passUsuario'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'emailUsuario'); ?>
		<?php echo $form->textField($model,'emailUsuario',array('size'=>60,'maxlength'=>180)); ?>
		<?php echo $form->error($model,'emailUsuario'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'verificarEmail'); ?>
		<?php echo $form->textField($model,'verificarEmail',array('size'=>60,'maxlength'=>180)); ?>
		<?php echo $form->error($model,'verificarEmail'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->