<?php
/* @var $this UsuariosController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Usuarios',
);

$this->menu=array(
	array('label'=>'Crear Usuarios', 'url'=>array('create')),
	//array('label'=>'Administrar Usuarios', 'url'=>array('admin')),

);
?>

<h1>Usuarios</h1>
<h6>De click sobre el Id Usuario para saber mas detalles.</h6>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
