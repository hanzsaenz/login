<?php
/* @var $this UsuariosController */
/* @var $data Usuarios */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idUsuario')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idUsuario), array('view', 'id'=>$data->idUsuario)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomUsuario')); ?>:</b>
	<?php echo CHtml::encode($data->nomUsuario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('apeUsuario')); ?>:</b>
	<?php echo CHtml::encode($data->apeUsuario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dirUsuario')); ?>:</b>
	<?php echo CHtml::encode($data->dirUsuario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userUsuario')); ?>:</b>
	<?php echo CHtml::encode($data->userUsuario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('passUsuario')); ?>:</b>
	<?php echo CHtml::encode($data->passUsuario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emailUsuario')); ?>:</b>
	<?php echo CHtml::encode($data->emailUsuario); ?>
	<br />


</div>